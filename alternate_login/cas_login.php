<?php
/**
 * Copyright 1999-2015 Horde LLC (http://www.horde.org/)
 *
 * See the enclosed file COPYING for license information (LGPL-2). If you
 * did not receive this file, see http://www.horde.org/licenses/lgpl.
 *
 * @author   Chuck Hagenbuch <chuck@horde.org>
 * @category Horde
 * @license  http://www.horde.org/licenses/lgpl LGPL-2
 * @package  Horde
 */

// Edit the following line to match the filesystem location of your Horde
// installation.

// CAS Login


require_once('CAS.php');

$HORDE_DIR = __DIR__;
require_once __DIR__ . '/lib/Application.php';

/* Initialize Horde environment. */
Horde_Registry::appInit('horde', array(
    'authentication' => 'none',
));



phpCAS::proxy(CAS_VERSION_2_0, $conf['cas']['host'], $conf['cas']['port'], $conf['cas']['context']);


if ( isset($conf['cas']['PGTStorageDb']['db']) ) {
    $db          = $conf['cas']['PGTStorageDb']['db'];
    $db_user     = $conf['cas']['PGTStorageDb']['user'];
    $db_password = $conf['cas']['PGTStorageDb']['pass'];
    $db_table    = $conf['cas']['PGTStorageDb']['table'];
    phpCAS::setPGTStorageDb($db, $db_user, $db_password, $db_table);
} else {
    phpCAS::setPGTStorageFile ($conf['cas']['PGTStorageDir']);
}


if (! empty($conf['cas']['cas_cacert'])) {
        phpCAS::setCasServerCACert ($conf['cas']['cacert']);
} else {
        phpCAS::setNoCasServerValidation();
}


phpCAS::forceAuthentication();

$username = phpCAS::getUser();
$password = phpCAS::retrievePT($conf['imap_service_name'] , $err_code, $output);

// Horde Login
$auth = $injector->getInstance('Horde_Core_Factory_Auth')->create();


// Check for CAS authentication.
if (empty($username) ||
    empty($password) ||
    !$auth->authenticate($username, array('password' => $password))) {
    $auth->getError();
}

require HORDE_BASE . '/index.php';
exit;

