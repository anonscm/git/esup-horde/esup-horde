
There is no automatic installation. Please follow the manual instructions given below ...

Step 1. Installing the phpCAS library
-------

This is a PEAR module, so it should be straightforward. 
Be aware this library needs the "curl" extension for PHP !

    pear  install  http://downloads.jasig.org/cas-clients/php/current.tgz

If you have problems, please consult https://wiki.jasig.org/display/CASC/phpCAS


Step 2. Installing the Horde Auth backend
-------

The necessary files are under auth-backend/

2.1. Copy "Cas.php" (the backend) under the PEAR Horde/Auth/ directory
Should be /usr/share/php/Horde/Auth/ if you haven't change the defaults
for PEAR or Horde.

2.2. Copy "casProxy.php" somewhere your web server can serve the file.
For example under the root Horde directory.
This file will be the callback URL used by the CAS server to transmit
the PGT

2.3. If you want to configure the backend through the Horde Administration interface,
put the file "cas-auth.xml" under horde/config/conf.d/ directory. Create that directory if you
haven't done it already.

2.4. If using clearPass, you just need to configure horde via admin interface now :)
Your CAS server has to allow https://yourwebmail/casProxy.php AND https://yourwebmail/ to be used 
as CAS proxy, and https://yourwebmail/casProxy.php to access clearPass

2.5. for SingleLogOut to work, you need a hook to get the sessionid associated with original ticket
  see examples/horde/config/hooks.php

Step 3. For renewal of the password ...
-------
If you are in the need of renewing the Proxy Ticket (i.e. the password stored in the 
Horde Session) after a connection failure. All files are under the auth-renew/ directory.

Apply *one* of the following methods, but not both !

o) Methode 1 : using runkit
~~~~~~~~~~~~

  !! Doesn't work if you use a "PHP accelerator" (apc, eAccelerator) !!

Please adapt the following paths according to you server installation.

3.1.1. Install the "runkit" PHP extension
     This extension is required to dynamically create "wrapper" functions against
     the IMAP and Sieve login functions.

    pecl install method_runkit/runkit-1.0.2.tgz
    echo extension=runkit.so > /etc/php5/conf.d/runkit.ini

3.1.2. Create the horde/lib/Cas/ directory

    mkdir /var/www/horde/lib/Cas/

3.1.3. Copy the wrapper methods under that directory

    cp method_runkit/RunkitRedefine_* /var/www/horde/lib/Cas/

o) Methode 2 : patching the source code statically
~~~~~~~~~~~~

  !! Works with PHP accelerators, but you need to apply (and possibly adapt) the patch
     at each Horde update !!

3.2.1. Patch the Horde/Imap/Client/Socket.php file in the PEAR installation directory

    cd /usr/share/php
    patch -p0 < PATH_TO/method_patch/Horde_Imap_Client_Socket.php.patch

3.2.2. Patch the ingo/lib/Transport/Timsieved.php in the Horde installation directory

    cd /var/www
    patch -p0 < PATH_TO/method_patch/ingo_lib_Transport_Timsieved.php.patch
