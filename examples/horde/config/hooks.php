<?php

class Horde_Hooks
{

      // postauthenticate for CAS SLO (SingleLogOut) to work:
      public function postauthenticate($userId, $credentials) {
        if (!empty($credentials['phpcassid'])) {
            Horde::logMessage("postauthenticate(".$userId.") phpcassid -> cache",'DEBUG');
            $cache = $GLOBALS['injector']->getInstance('Horde_Cache');
            $sids = $cache->get('cas_st_sid', 86400);
            if ($sids) $sts=unserialize($sids);
            else $sts=array();
            $sts[$credentials['phpcassid']]=session_id();
            $cache->set('cas_st_sid',serialize($sts),86400);
            Horde::logMessage("postauthenticate(".$userId.") phpcassid -> cache DONE :)",'DEBUG');
        } else {
            Horde::logMessage("postauthenticate(".$userId.") pas de phpcassid dans les credentials",'INFO');
        }
        // recupere les attributs CAS s'il y en a
        if (isset($credentials['attrs'])) {
            $_SESSION['CASattrs']=$credentials['attrs'];
        }
        return true;
      }

    // use CAS attributes from postauthenticate (if any)
    public function prefs_init($pref, $value, $username, $scope_ob)
    {
        // map: horde_pref => cas attribute name
        $map=array('from_addr'=>'mail',
                   'fullname'=>'displayName',
                   'location'=>'postalAddress');

        if (isset($_SESSION['CASattrs'][$map[$pref]])) {
            // replace '$' by ', ' in postalAddress
            if ($pref === "location") {
                return preg_replace('/\$/',', ',$_SESSION['CASattrs'][$map[$pref]]);
            }
            return $_SESSION['CASattrs'][$map[$pref]];
        }
        // else return original value
        return $value;
    }

}
