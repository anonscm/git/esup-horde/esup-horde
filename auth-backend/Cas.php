<?php
/**
 * The Horde_Auth_Cas class provides transparent authentication
 * through the CAS Web SSO (http://www.jasig.org/cas).  
 *
 * Copyright Consortium Esup-Portail (http://www.esup-portail.org)
 *
 * See the enclosed file COPYING for license information (LGPL). If you did
 * not receive this file, see http://opensource.org/licenses/lgpl-2.1.php
 *
 * @author   Julien Marchal <julien.marchal@univ-nancy2.fr>
 * @author   Xavier Montagutelli <xavier.montagutelli@unilim.fr>
 * @category Horde
 * @license  http://opensource.org/licenses/lgpl-2.1.php LGPL
 * @package  Auth
 */

/* Load the phpCAS module */
include_once('CAS.php');

class Horde_Auth_CAS extends Horde_Auth_Ldap
{
    /**
     * An array of capabilities, so that the driver can report which
     * operations it supports and which it doesn't.
     *
     * @var array
     */
    protected $_capabilities = array(
        'transparent' => true,
        'list'        => true,
    );

    protected $_cas_version;
    protected $_session;
    protected $_phpcas_session;

    /** 
     * Name of the imap service (something like imap://imap.corp.com), to retrieve a PT from CAS
     *
     * @var string
     */
    protected $_imapService = "";

    /**
     * Cache Ticket <-> sessionID for CAS global logout
     */
    const cas_st_sid_list = 'cas_st_sid';

    /**
     * Constructor.
     *
     * @param array $params  Parameters:
     * <pre>
     * 'password_header' - (string) Name of the header holding the password of
     *                     the logged in user.
     * 'password_holder' - (string) Where the hordeauth password is stored.
     * 'password_preference' - (string) Name of the Horde preference holding
     *                         the password of the logged in user.
     * 'username_header' - (string) [REQUIRED] Name of the header holding the
     *                     username of the logged in user.
     * </pre>
     *
     * @throws InvalidArgumentException
     */
    public function __construct(array $params = array())
    {
        foreach (array('cas_host') as $val) {
            if (!isset($params[$val])) {
                throw new InvalidArgumentException(__CLASS__ . ': Missing ' . $val . ' parameter.');
            }
        }

        // Default values are merged with configuration supplied values
        $params = array_merge(array(
            'debuglevel' => 'DEBUG',            // debug messages will be emitted with that loglevel
            'cas_host' => '',       
            'cas_port' => '443',
            'cas_context' => '/cas',
            'cas_version' => 'CAS_VERSION_2_0',
            'cas_clearpass' => false,
            'cas_clearpass_uri' => '/clearPass',
            'cas_proxy' => false,               // acts as a CAS proxy ?
            'cas_cacert' => '',
            'cas_pgtdir' => session_save_path(),
            'cas_callback_url' => '',
            'cas_handle_logout' => true,
            'cas_handle_logout_hosts' => array(),
            'cas_cache_sid_logout' => true,	// use horde cache to permit logout ?
            'uid' => 'uid',
        ), $params);


        // Horde_Auth_Ldap expects 'ldap' param to be Horde_Ldap object.
        // When using Horde_Auth_Ldap for authentication, the user id/password are used,
        // in 'preauthenticate' hook.
        // Here we create a simple Horde_Ldap object
        $this->_debug('Creating the ldap object');
        $params['ldap'] = new Horde_Ldap($params['ldap']);

        parent::__construct($params);
        $this->_debug('Building new object, URI ' . $_SERVER['REQUEST_URI']);

        // Store ticket in phpCAS session (for future logout)
        if(!empty($_GET['ticket'])) {
            $this->_debug('Keep original ST '.$_GET['ticket'].' session for now');
            $_SESSION['phpCAS']['ST']=$_GET['ticket'];
        }

        // Restore possible previous arguments
        $this->__get_arg();
   
        // Initialize phpCAS library 
        // For the CAS global logout : the PHP session ID should be named with the ticket ID
        if (! empty($this->_params['cas_debug'])) {
            phpCAS::setDebug($this->_params['cas_debug']);
        }

        eval("\$this->_cas_version=".$this->_params['cas_version'].";");

        if ($this->_params['cas_proxy']) {
            $this->_debug('CAS Proxy mode');
            if (empty($this->_params['cas_callback_url']))
                throw new InvalidArgumentException(__CLASS__ . ': Missing cas_callback_url parameter.');

            phpCAS::proxy($this->_cas_version, $this->_params['cas_host'], $this->_params['cas_port'], $this->_params['cas_context'], false);
            phpCAS::setFixedCallbackURL ($this->_params['cas_callback_url']);
            phpCAS::setPGTStorageFile ($this->_params['cas_pgtdir']);

            if ($this->_params['cas_handle_logout']) {
                // To handle logout correctly the service URL needs to be fixed
                phpCAS::setFixedServiceURL(Horde::url('', true)->url);
            } else {
                // Remove trailing "/" for the service URL. Sometimes, the CAS ticket is not validated
                // because the URL does not match the service. They differ with this trailing slash... WHY ?
                $my_url = phpCAS::getServiceURL();
                if (preg_match('&/$&', $my_url)) {
                    $my_url = preg_replace('&/$&','', $my_url);
                    phpCAS::setFixedServiceURL($my_url);
                }
            }
        } else {
            $this->_debug('CAS Client mode');
            phpCAS::client($this->_cas_version, $this->_params['cas_host'], $this->_params['cas_port'], $this->_params['cas_context'], false);
        }

        if (! empty($this->_params['cas_cacert'])) {
            $this->_debug('Verifying cas server certificate with the CA file ' . $this->_params['cas_cacert']);
            phpCAS::setCasServerCACert ($this->_params['cas_cacert']);
        } else {
            $this->_debug('Not verifying the cas server certificate. Not recommended!');
            phpCAS::setNoCasServerValidation();
        }

        // Handle global logout sent by the CAS server
        // "phpCAS can't handle logout requests if it does not manage the session." so we have to register a callback function
        phpCAS::setSingleSignoutCallback (array($this, 'globalLogout'));
        phpCAS::handleLogoutRequests(
                 $this->_params['cas_handle_logout'], 
                 empty($this->_params['cas_handle_logout_hosts']) ? array($this->_params['cas_host']) : $this->_params['cas_handle_logout_hosts']
         );

        $vars = Horde_Variables::getDefaultVariables();
        $this->_logout = $vars->logout_reason;
        $this->_debug('_logout=' . $this->_logout);

        // Hack - Rewrite some PEAR methods to renew password (i.e. proxy ticket) when authentication fails
        // Switchable (on/off) by configuration
        // Only needed when in proxy mode
        if ($this->_params['cas_proxy'] && $this->_params['cas_renew_password']) {
            $this->_debug('Incorporating wrapper functions to renew the Proxy Ticket when IMAP or Sieve login fails (hack).');
            if (! function_exists("runkit_method_rename")) 
                throw new InvalidArgumentException(__CLASS__ . ': parameter "cas_renew_password" is TRUE, but the PHP runkit extension needed by this hack is not available!');
            $this->_wrapImapLoginMethod();
            $this->_wrapTimsievedLoginMethod();
        }

        $this->_debug('Done, new object instantiated');
    }

    /**
     * Authentication stub.
     *
     * On failure, Horde_Auth_Exception should pass a message string (if any)
     * in the message field, and the Horde_Auth::REASON_* constant in the code
     * field (defaults to Horde_Auth::REASON_MESSAGE).
     *
     * @param string $userID      The userID to check.
     * @param array $credentials  An array of login credentials.
     *
     * @throws Horde_Auth_Exception
     */
    protected function _authenticate($userId, $credentials)
    {
        throw new Horde_Auth_Exception('Unsupported.');
    }

    /**
     * Automatic authentication: through the CAS server.
     *
     * @return boolean  Whether transparent login is supported.
     * @throws Horde_Auth_Exception
     *
     */
    public function transparent()
    {
        $this->_debug('Starting');
        if (! phpCAS::isAuthenticated()) {
            $this->_debug('NOT authenticated, forcing CAS authentication');
            phpCAS::forceAuthentication();
        }
        $username = phpCAS::getUser();
        if (empty($username)) {
            $this->_log_error('We are supposed to be authenticated by CAS, but no username returned!');
            throw new Horde_Auth_Exception('No username returned by CAS!');
        }
        $this->_debug('Authenticated as user ' . $username);

        // Remove scope from username, if present.
        $this->setCredential('userId', $this->_removeScope($username));

        // Set password for hordeauth login. Only when in proxy mode.
        if ($this->_params['cas_proxy']) {
            $this->_debug('Getting a proxy ticket as the Horde password');
            if ($this->_params['cas_clearpass']) {
                if (!$this->_getClearPassword()) {
                    $this->_log_error("Error getting clearPass");
                    $this->setError(Horde_Auth::REASON_MESSAGE,"Impossible de se connecter pour l'instant");
                    return false;
                }
            } else if (! $this->_getNewPassword()) {
                $this->_log_error('Error getting Proxy Ticket!');
                $this->setError(Horde_Auth::REASON_MESSAGE,"Impossible de se connecter pour l'instant");
                return false;
            }
        }
	
	$this->setCredential('credentials', array(
            'phpcasuser' => $_SESSION['phpCAS']['user']
        ));
	$this->setCredential('credentials', array(
            'phpcaspgt' => $_SESSION['phpCAS']['pgt']
        ));
        if (array_key_exists('ST',$_SESSION['phpCAS'])) {
            $this->setCredential('credentials', array(
                'phpcassid' => $_SESSION['phpCAS']['ST']
            ));
        }
        if (phpCAS::hasAttributes()) {
            $attrs=phpCAS::getAttributes();
            $this->setCredential('credentials', array(
                'attrs' => $attrs
            ));
        }

        return true;
    }

    /**
     * Checks for triggers that may invalidate the current auth.
     * These triggers are independent of the credentials.
     * Verifie si l'authentification CAS est toujours valide 
     *
     * @return boolean  True if the results of authenticate() are still valid.
     */
    public function validateAuth()
    {

	$_SESSION['phpCAS']['user'] = $GLOBALS['registry']->getAuthCredential('phpcasuser');
	$_SESSION['phpCAS']['pgt'] = $GLOBALS['registry']->getAuthCredential('phpcaspgt');
        
	return phpCAS::isAuthenticated();
    }


    /**
     * Callback function called when the CAS server sends a "global logout" request
     * This function is not called by the user or its browser, but from the CAS server!
     *
     * The CAS POSTs a SAML message containing the ST initially delivered to Horde for a user disconnecting from CAS
     * We have previously renamed the user session id with the ST => here, we create a new PHP session with that
     * "secret" and we destroy it. Code stolen from the phpCAS library.
     *
     * @param string $ticket2logout CAS Service Ticket linked to a user that is disconnecting from CAS server
     *
     */
    function globalLogout($ticket2logout)
    {
        $this->_debug('Global Logout called for ticket ' . $ticket2logout);

        // Get corresponding horde session id from horde cache
        if ($this->_params['cas_cache_sid_logout']) {
            $cache = $GLOBALS['injector']->getInstance('Horde_Cache');
            $sids = $cache->get($this::cas_st_sid_list, 86400);
            if (!empty($sids)) {
                $this->_debug('Looking for ' . $ticket2logout. ' in cache');
                $sts=unserialize($sids);
                if (array_key_exists($ticket2logout,$sts)) {
                    $this->_debug('Found ' . $ticket2logout. ': matches '.$sts[$ticket2logout]);
                    $session_id = $sts[$ticket2logout];
                    unset($sts[$ticket2logout]);
                    $cache->set($this::cas_st_sid_list, serialize($sts), 86400);
                }
            }
        }

        // if session had been sucessfully renamed (possible???)
        if (empty($session_id))
            $session_id = preg_replace('/[^\w]/','',$ticket2logout);

        // destroy a possible application session created before phpcas
        if(session_id() !== ""){
            session_unset();
            session_destroy();
        }
        $this->_debug('Logout: destroying session '.$session_id);
        // fix session ID
        session_id($session_id);
        $_COOKIE[session_name()]=$session_id;
        $_GET[session_name()]=$session_id;

        // Overwrite session
        session_start();
        session_unset();
        session_destroy();
        $this->_debug('Logout: done.');
    }

    /**
     * -=-=-=- Private methods -=-=-=-=-
     */

    /**
     * Log messages for debugging
     *
     */
    protected function _debug($message)
    {
        if (! isset($this->_logger)) return;
        $call = debug_backtrace(false);
        $call = (isset($call[1]))?$call[1]:$call[0];
        $header = __CLASS__ . '::' . $call['function'] . ' ' . $_SERVER['REMOTE_ADDR'] . ' [' . session_id() . '][' . $this->_session . ']';
        $this->_logger->log($header . $message, $this->_params['debuglevel']);
    }
    protected function _log_error($message)
    {
        if (! isset($this->_logger)) return;
        $call = debug_backtrace(false);
        $call = (isset($call[1]))?$call[1]:$call[0];
        $header = __CLASS__ . '::' . $call['function'] . ' ';
        $this->_logger->log($header . $message, 'ERR');
    }

    /**
     * Removes the scope from the user name, if present.
     *
     * @param string $username  The full user name.
     *
     * @return string  The user name without scope.
     */
   protected function _removeScope($username)
   {
       $pos = strrpos($username, '@');
       return ($pos !== false)
           ? substr($username, 0, $pos)
           : $username;
   }

   /**
    * Rewrite dynamically some PEAR methods ...
    * Hack (Bad Thing) to renew the PT (password) in case of authentication failure
    * inside a Horde App like IMP when connecting an IMAP server.
    */
    protected function _wrapImapLoginMethod()
    {
        $filename = HORDE_BASE . '/lib/Cas/RunkitRedefine_Horde_Imap_Client_Socket::_tryLogin.php';
        $imapLogin = file_get_contents($filename);
        if (! $imapLogin || empty ($imapLogin)) {
            $this->_log_error('Error reading file containing wrapper Horde_Imap_Client_Socket::_tryLogin method!');
            throw new InvalidArgumentException(__CLASS__ . ': parameter "cas_renew_password" is TRUE, but the wrapper function for the Imap Login function is not available!');
            return false;
        }

        include_once('Horde/Imap/Client/Socket.php'); // To load the class and the method to redefine
        if (! runkit_method_rename('Horde_Imap_Client_Socket', '_tryLogin', '__tryLogin')) {
            $this->_log_error('Error renaming Horde_Imap_Client_Socket::_tryLogin method!');
            return false;
        }
        if (! runkit_method_add('Horde_Imap_Client_Socket', '_tryLogin', '$method', $imapLogin)) {
            $this->_log_error('Error adding wrapper Horde_Imap_Client_Socket::_tryLogin method!');
            return false;
        }
        $this->_debug('Wrapper for method Horde_Imap_Client_Socket::_tryLogin loaded');
        return true;
    }

    protected function _wrapTimsievedLoginMethod()
    {
        $filename = HORDE_BASE . '/lib/Cas/RunkitRedefine_Ingo_Transport_Timsieved::_connect.php';
        $timsievedConnect = file_get_contents($filename);
        if (! $timsievedConnect || empty ($timsievedConnect)) {
            $this->_log_error('Error reading file containing wrapper Ingo_Transport_Timsieved::_connect method!');
            throw new InvalidArgumentException(__CLASS__ . ': parameter "cas_renew_password" is TRUE, but the wrapper function for the Seieve Login function is not available!');
            return false;
        }

        include_once(HORDE_BASE . '/ingo/lib/Transport.php'); // To load the class and the method to redefine
        include_once(HORDE_BASE . '/ingo/lib/Transport/Timsieved.php'); // To load the class and the method to redefine
        if (! runkit_method_rename('Ingo_Transport_Timsieved', '_connect', '__connect')) {
            $this->_log_error('Error renaming Ingo_Transport_Timsieved::_connect method!');
            return false;
        }
        if (! runkit_method_add('Ingo_Transport_Timsieved', '_connect', '', $timsievedConnect)) {
            $this->_log_error('Error adding wrapper Ingo_Transport_Timsieved::_connect method!');
            return false;
        }
        $this->_debug('Wrapper for method Ingo_Transport_Timsieved::_connect loaded');
        return true;
    }

    /**
     * Get a new Proxy Ticket, stored as the password for Horde (and other apps)
     *
     * @access public
     *
     * @return string  The new password, or false
     */
    protected function _getNewPassword($service='')
    {
        if (! phpCAS::isAuthenticated()) {
            $this->_log_error('Trying to renew CAS Proxy Ticket, but phpCAS says we are not authenticated!');
            return false;
            // $this->_refreshAuth();
        }

        if (empty ($service)) {
            if (empty($this->_imapService)) $this->_getImapService();
            $service = $this->_imapService;
        }
        $pt = $this->_getPT($service);
        $this->_debug('New Proxy Ticket ' . $pt . ' for user ' . phpCAS::getUser());
        $this->setCredential('credentials', array( 'password' => $pt));
        //?? Auth::clearAuth();
        return $pt;
    }

    protected function _getPT($service, $refresh=false)
    {
        $pt = phpCAS::retrievePT($service, $err_code, $output);
        if (empty($pt) || 
            (!preg_match('/^PT-/',$pt) && !preg_match('/^ST-/',$pt)) ) {
            $this->_log_error('User ' . phpCAS::getUser() . " Bad or empty Proxy Ticket ($pt) ! phpCAS error : " . $output);
            $this->_refreshAuth();
            throw new Horde_Auth_Exception("Bad or empty Proxy Ticket. You must login again ?");
        } 
        $this->_debug('User ' . phpCAS::getUser() . " retrieve Proxy Ticket $pt");
        return($pt);
    }

    /**
     * Get the user's password from clearPass extension to horde's credantials
     *
     * @access private
     *
     * @return bool  status
     */
    protected function _getClearPassword() {
        $port_str = ($this->_params['cas_port'] == '443')?'':':'.$this->_params['cas_port'];
        $service = 'https://'.$this->_params['cas_host'].$port_str.$this->_params['cas_context'].$this->_params['cas_clearpass_uri'];
        $this->_debug('ClearPass request for user '. phpCAS::getUser());
        if (phpCAS::serviceWeb($service,$err_code,$output)) {
            if (($err_code !== PHPCAS_SERVICE_OK)||($output==="")) {
                $this->_log_error('_getClearPassword() '.$service.' NOK('.$output.')');
                return false;
            }
            try {
                $xml = simplexml_load_string($output);
                $namespaces = $xml->getNameSpaces(true);
                $cas = $xml->children($namespaces['cas']);
            } catch (Horde_Auth_Exception $e) {
                $this->_log_error('User ' . phpCAS::getUser() . ' ClearPass failed to load xml response : '.$output);
//                throw new Horde_Auth_Exception("Erreur du systeme d'authentification");
                return false;
            }
            if(isset($cas->clearPassSuccess)) {
                $this->setCredential('credentials', array( 'password' => (string)$cas->clearPassSuccess->credentials ));
                $this->_debug('User ' . phpCAS::getUser() . " retrieve User's password");
            } else {
                //throw new Horde_Auth_Exception('User ' . phpCAS::getUser() . ' ClearPass != Success :(');
                $this->_log_error('User ' . phpCAS::getUser() . ' ClearPass != Success :(');
                return false;
            }
        } else {
            //throw new Horde_Auth_Exception('User ' . phpCAS::getUser() . ' ClearPass service failed ! phpCas error : '.$output);
            $this->_log_error('User ' . phpCAS::getUser() . ' ClearPass service failed ! phpCas error : '.$output);
            return false;
        }
        return true;
    }

    protected function _refreshAuth() {
        // Hack to force phpCAS renewal ...
        $this->_debug('Starting');
        unset ($_SESSION['phpCAS']);
        $GLOBALS['registry']->clearAuth();
        $this->__sav_arg();
        phpCAS::renewAuthentication();
    }

    /**
     *
     */
    protected function _getImapService()
    {
        @define ('IMP_BASE', HORDE_BASE . '/imp');
        require_once IMP_BASE . '/lib/Auth.php';
        require_once IMP_BASE . '/lib/Imap.php';
        require_once IMP_BASE . '/lib/Imap/Config.php';
        require_once IMP_BASE . '/lib/Mailbox.php';
        $servers = Horde::loadConfiguration('backends.php', 'servers', 'imp');


        $server = IMP_Auth::getAutoLoginServer();

        if (empty($server)) return false;

        $p = preg_replace("'/(.*)$'" , "" , $servers[$server]['protocol']);
        $host = empty($servers[$server]['realhost']) ? $servers[$server]['hostspec'] : $servers[$server]['realhost'];
        $this->_imapService = $p . 's://' . $host;
    	$this->_debug('User ' . phpCAS::getUser() . ', IMAP server key ' . $server . ' (web server name ' . $_SERVER['SERVER_NAME'] . '), IMAP service ' . $this->_imapService);
        return $this->_imapService;
    }

    /**
     * Save the URL arguments in a temporary file
     * For future use after authentication
     */
    protected function __sav_arg() 
    {
        if (empty($_GET['pgtId']) &&
            empty($_SESSION['cas']['__SAVARG']) ) {

            if(!empty($_GET)  && is_array($_GET))  $SAV['_GET']  = $_GET;
            if(!empty($_POST) && is_array($_POST)) $SAV['_POST'] = $_POST;

            $_SESSION['cas']['__SAVARG_FILE'] = $this->__tempFilePath();

            $fp = @fopen($_SESSION['cas']['__SAVARG_FILE'], 'w');
            if(!$fp) {
                $this->_log_error('Failed to open save argument file : ' . $_SESSION['cas']['__SAVARG_FILE']);
                unset($_SESSION['cas']['__SAVARG_FILE']);
                return(false);
            }
            fwrite( $fp, serialize($SAV));
            fclose($fp);
        }
    }

    protected function __get_arg() 
    {
        if (empty($_SESSION['cas']['__SAVARG_FILE']) ||
            ! file_exists($_SESSION['cas']['__SAVARG_FILE'])) {
            unset ($_SESSION['cas']['__SAVARG_FILE']);
            return false;
        }

        $serial_sav = file_get_contents($_SESSION['cas']['__SAVARG_FILE']);
        if (! $serial_sav || empty($serial_sav)) {
                $this->_log_error('Failed to read content of save argument file : ' . $_SESSION['cas']['__SAVARG_FILE']);
                unset($_SESSION['cas']['__SAVARG_FILE']);
                return false;
        }
        $SAV = unserialize($serial_sav);
        @unlink($_SESSION['cas']['__SAVARG_FILE']);

        if(!empty($SAV['_GET']) && is_array($SAV['_GET'])) {
            reset ($SAV['_GET']);
            while (list ($key, $val) = each ($SAV['_GET'])) {
                if(empty($_GET[$key])){
                    if($key == 'url') {
                        $val = preg_replace("/&amp;/","&",$val);
                        $val = Horde::applicationUrl($val,true,false);
                    }
                    $_GET[$key] = $val;
                }
            }
        }

        if(!empty($SAV['_POST']) && is_array($SAV['_POST'])) {
            reset ($SAV['_POST']);
            while (list ($key, $val) = each ($SAV['_POST'])) {
                if(empty($_POST[$key])) {
                    if($key == 'url') {
                        $val = preg_replace("/&amp;/","&",$val);
                        $val = Horde::applicationUrl($val,true,false);
                    }
                    $_POST[$key] = $val;
                }
            }
        }
        $_SESSION['cas']['__SAVARG'] = '';
        unset($_SESSION['cas']['__SAVARG']);
    }

    protected function __tempFilePath() 
    {
        return Horde::getTempFile('cas_', false);
    }

}
